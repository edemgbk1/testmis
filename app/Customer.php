<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //


    protected $table = 'customers';

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'Name', 'Email','Phone'
  ];



  public function expenses()
  {
      return $this->belongsToMany(Expense::Class);
  }

  //     public function expenses()
  // {
  //     return $this->morphToMany('App\Expense','expendable');
  // }

}
